import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username?: string;

  constructor(private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    /**
     * On vérifie le statut isLoggedIn à l'aide de TokenStorageService, s'il est vrai, on obtient les rôles de l'utilisateur et
     * on deffinit la valeur de showAdminBoard et showModeratorBoard.
     * Ils contrôleront la façon dont la barre de navigation du modèle affiche ses éléments.
    */
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

      this.username = user.username;
    }
  }

  //lien vers le bouton de déconnexion qui appelle la méthode logout() et recharge la fenêtre.
  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
